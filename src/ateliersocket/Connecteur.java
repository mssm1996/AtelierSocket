package ateliersocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Connecteur{
    private Socket socket; 
    private OutputStream ecrivain; // flux d'ecriture
    private InputStream lecteur; // flux de lecture
    
    public Connecteur(String adresseServeur,int port){
        try{
            this.socket = new Socket(adresseServeur,port);
            this.lecteur = this.socket.getInputStream();
            this.ecrivain = this.socket.getOutputStream();
        }
        catch(IOException exp){
            exp.printStackTrace();
        }
    }
    
    public void Envoyer(String requete){
        requete += "\n";//caractère de fin de ligne: Pour que le serveur puisse lire une ligne au complet
        try{
            this.ecrivain.write(requete.getBytes()); // on envoie xChose sous forme d'un tableau de type byte
            this.ecrivain.flush();// Pour être sure que les données ont été envoyé
        }
        catch(IOException exp){
            exp.printStackTrace();
        }
    }
    
    public String Lire(){
        String reponse = "";
        try{
            byte[] flux = new byte[4096];// lire 4ko d'un seul coup (4ko = 4096 octets)
            int taille = this.lecteur.read(flux); // la lecture + taille = nombre de caractères lus
            reponse = new String(flux,0,taille); // on m'est tout ça dans un string (chaine de caractères)
        }
        catch(IOException exp){
            exp.printStackTrace();
        }
        return reponse;
    }
    
    public void FermerConnexion(){
        try{
            this.lecteur.close();
            this.ecrivain.close();
            this.socket.close();
        }
        catch(IOException exp){
            exp.printStackTrace();
        }
    }
}
