package ateliersocket;
public class AtelierSocket {
    public static void main(String[] args) {
        //----- Connexion au serveur -----
        Connecteur c = new Connecteur("192.168.43.209",4568);
        
        //----- Envoie d'une requête -----
        c.Envoyer("get date");
        System.out.println(c.Lire());
        
        //----- Fermeture de la connexion ----- 
        c.Envoyer("close connection");
        System.out.println(c.Lire());
        c.FermerConnexion();
    }    
}
